import React from "react";
import AdvancedGameList from "./components/AdvancedGameList"
import "./reset-style.css"
import "./styles/fonts.css"
import "./App.css"
import { Link, Routes, Route } from "react-router-dom";
import AllGames from "./routes/AllGames";
import NewGame from "./routes/NewGame";

const App = () => {
	return (
		<>
			<header className="site-header">
				<div className="navigation">
					<Link to="/">
						<h1>Untitled Board Game Planner</h1>
					</Link>
					<nav>
						<Link to="/all-games">Game List</Link> | {" "}
						<Link to="/add-game">Add Game</Link>
					</nav>
				</div>
				<img alt="A Goose holding keys" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJ0-eTCizNbDN_8yisevnuF3E_HNAguAcqzQ&usqp=CAU"/>
			</header>
			<div className="content">
				<Routes>
					<Route path="/" element={<AdvancedGameList/>}/>
					<Route path="/all-games" element={<AllGames/>}/>
					<Route path="/add-game" element={<NewGame/>}/>
					<Route path="*" element={<p>You Fucked Up</p>}/>
				</Routes>
			</div>
		</>
	)
}

export default App;