import React, { useCallback, useState } from 'react';
import { GENRE_OPTIONS } from "../docs/genres";
import CreatableSelect from 'react-select/creatable';
import FilterInput from "../components/FilterInput";
import MinMaxInput from "../components/MinMaxInput";
import LimitedTextArea from '../components/LimitedTextArea';
import axios from "axios";
import LargeTextArea from '../components/LargeTextArea';

const NewGame = () => {
	const [name, setName] = useState("");
	const [minPlayers, setMinPlayers] = useState("");
	const [maxPlayers, setMaxPlayers] = useState("");
	const [description, setDescription] = useState("");
	const [longDescription, setLongDescription] = useState("");
	const [selectedGenres, setSelectedGenres] = useState([]);

	const resetForm = () => {
		setName("");
		setMinPlayers("");
		setMaxPlayers("");
		setDescription("");
		setLongDescription("");
	}

	const addNewGame = () => {
		const data = {
			name,
			minPlayers,
			maxPlayers,
			description,
			longDescription,
			genres: selectedGenres.map(genre => genre.value)
		}
		console.log(data);
		
		axios.post("/api/games", data).then((response) => {
			alert(response.data.message)
		}).catch((err) => {
			alert(err.response.data.message)
		})
	}

	const onGenreSelect = (newValue) => {
		setSelectedGenres(newValue)
	}

	return (
		<main>
			<form>
				<h2>Add a New Game</h2>
				<FilterInput
					label="Game"
					type="text" 
					placeholder="New Game Title..."
					value={name}
					onChange={setName}
				/>
				<MinMaxInput
					label="No. of players"
					minPlaceholder="Min. Players"
					maxPlaceholder="Max. Players"
					minValue={minPlayers}
					maxValue={maxPlayers}
					onMinChange={setMinPlayers}
					onMaxChange={setMaxPlayers}
				/>
				<div className="category-list">
					<label>Categories</label>
						<CreatableSelect
							isMulti
							options={GENRE_OPTIONS}
							classNamePrefix="tomiscool"
							onChange={onGenreSelect}
						/>
				</div>

				<LimitedTextArea
					label="Short Description"
					value={description}
					onChange={setDescription}
				/>

				<LargeTextArea
					label="Long Description"
					value={longDescription}
					onChange={setLongDescription}
				/>

			<button 
				type="reset" 
				className="form-button"
				onClick={() => {
					resetForm()
				}}
			>
				Reset
			</button>
			<button 
				type="submit" 
				className="form-button"
				onClick={(event) => {
					event.preventDefault()
					addNewGame()
				}}
				disabled={
					name === "" ||
					minPlayers === "" ||
					maxPlayers === "" ||
					description === "" ||
					longDescription === ""
				}
			>
				Submit
			</button>
			</form>

		</main>
	);
	}

export default NewGame;