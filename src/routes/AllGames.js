import React, { useState, useEffect } from "react";
import Select from 'react-select'
import BasicGameList from "../components/BasicGameList"
import AZButton from "../components/AZButton"
import CategoryList from "../components/CategoryList"
import LimitedTextarea from "../components/LimitedTextArea";
import { GENRE_OPTIONS } from "../docs/genres";
import axios from "axios";
import Modal from "../components/Modal"
import css from "../styles/AllGameListItem.module.css"

const AllGames = () => {

  const [letterFilter, setLetterFilter] = useState("");
  return (
    <main style={{ padding: "1rem 0" }}>
      <h2>All Games</h2>
        <AZButton
          onLetterClicked={(letter) => setLetterFilter(letter)}
          letterFilter={letterFilter}
        />
      	<BasicGameList
          letterFilter={letterFilter}
        />
      <br/>

      <br/>
    </main>
  );
}

export default AllGames;