export const GENRE_OPTIONS = [
      { value: 'strategy', label: 'Strategy' },
      { value: 'resource gathering', label: 'Resource Gathering' },
      { value: 'worker placement', label: 'Worker Placement' },
      { value: 'deck building', label: 'Deck Building' },
      { value: 'party', label: 'Party' },
      { value: 'engine building', label: 'Engine Building' },
      { value: 'card', label: 'Card' },
      { value: 'bluffing', label: 'Bluffing' },
      { value: 'dexterity', label: 'Dexterity' },

  ];
  
  export const groupedOptions = [
    {
      label: 'Genre',
      options: GENRE_OPTIONS,
    },
  ];