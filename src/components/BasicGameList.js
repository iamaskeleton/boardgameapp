import React, { useState, useEffect } from "react";
import AllGameListItem from "./AllGameListItem";
import GameDetails from "./GameDetails";
import axios from "axios";
import Modal from "./Modal"; // components are Capitalised

const BasicGameList = (props) => {
	const { letterFilter } = props

	const [isLoading, setIsLoading] = useState(true); // <-- react hook - allows you to create variables that stick e.g. adding text to an input
	const [games, setGames] = useState([]); // <-- react hook
	const [isModalOpen, setIsModalOpen] = useState(false);
	const [modalGameName, setModalGameName] = useState(""); // better to have empty string than "null" because then it will just write null
	const [modalGameSmallDescription, setModalGameSmallDescription] = useState("");
	const [modalGameID, setModalGameID] = useState(null);

	useEffect(() => { // <-- react hook - allows you to run functions on re-renders
		axios.get("/api/games").then((response) => { //response is what comes back from the server
			setIsLoading(false);

			const gameList = [...response.data]  //creates a copy of response.data array - ... is a Spread Operator (js)
			gameList.sort((a, b) => {
				if (a.name > b.name) {
					return 1 //return means "its done" and it returns a value
				}
				if (a.name < b.name) {
					return -1
				}
				return 0
			})
			setGames(gameList); //this sets the sorted copy to the variable(state) "games"
		}).catch((err) => {
			setIsLoading(false);
			alert("something went wrong");
		})
	}, []) // {} - is a way of tracking whats changing and when to run useEffect e.g. if "games" was inside the brackets the it would run the function when games changes. When it's empty it runs once when the componenet mounts (dependencies).

	const setModalData = (game) => {
		setIsModalOpen(true)
		setModalGameName(game.name)
		setModalGameSmallDescription(game.description)
		setModalGameID(game._id)
	}

	const filteredGames = letterFilter === "" ? games : games.filter(game => game.name.charAt(0).toUpperCase() === letterFilter) // ? - shorthand for if else 

	return (
		isLoading ? (
			<p>loading...</p>
		) : (
			<>
				<Modal
					isOpen={isModalOpen}
					onCloseButtonClick={() => {
						setIsModalOpen(false)
						setModalGameID(null)
					}}
				>
					<h1>{modalGameName}</h1>
					<h2>{modalGameSmallDescription}</h2>
					<GameDetails
						gameID={modalGameID} // gameID is a property of GameDetails. Here, we are passing the modalGameID info through to the gameID property within GameDetails
					/>
				</Modal>
				<ul>
					{filteredGames.map(game => (
						<AllGameListItem
							onButtonClick={() => setModalData(game)} // () => - means start function
							key={game._id}
							name={game.name}
						/>
					))}
				</ul>
			</>
		)
	)
}

export default BasicGameList;