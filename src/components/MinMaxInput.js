import React from "react";

const MinMaxInput = (props) => {
	const {
		label, 
		minPlaceholder, 
		maxPlaceholder, 
		minValue,
		maxValue,
		onMinChange,
		onMaxChange
	} = props;
	return(
		<div className="form__field">
			<label>{label}</label>
			<div className="form__field-min-max">
				<input 
					type="number" 
					placeholder={minPlaceholder}
					value={minValue}
					onChange={
						(event) => onMinChange(event.target.value)
					}
				/>
				<input 
					type="number"
					placeholder={maxPlaceholder}
					value={maxValue}
					onChange={
						(event) => onMaxChange(event.target.value)
					}
				/>
			</div>
		</div>
	)
}

export default MinMaxInput;