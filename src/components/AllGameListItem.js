import React, { useEffect, useState } from "react";
import css from "../styles/AllGameListItem.module.css";
import Modal from "./Modal";

const AllGameListItem = (props) => {

	const {name, onButtonClick} = props;

		return (
		<li className={css.AllGameListItem}>
				<button
					type="button"
					onClick={(() => onButtonClick(name))}
				>
				{name}
				</button>

		</li>
	)
		
}



export default AllGameListItem;