import React, { Component } from 'react';
import CreatableSelect from 'react-select/creatable';
import { GENRE_OPTIONS } from "../docs/genres";

export default class GenreSelectList extends Component {
  handleChange = (
    newValue,
    actionMeta
  ) => {
    console.group('Value Changed');
    console.log(newValue);
    console.log(`action: ${actionMeta.action}`);
    console.groupEnd();
  };
  render() {
    return (
      <CreatableSelect
        isMulti
        onChange={this.handleChange}
        options={GENRE_OPTIONS}
      />
    );
  }
}