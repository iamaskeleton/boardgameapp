import React from "react";

const FilterInput = (props) => {
	const {label, type, placeholder, value, onChange, ...otherProps} = props;
	return(
		<div className="form__field">
			<label>{label}</label>
			<input 
				type={type} 
				placeholder={placeholder}
				value={value}
				onChange={
					(event) => onChange(event.target.value)
				}
				{
					...otherProps
				}
			/>
		</div>
	)
}

export default FilterInput;