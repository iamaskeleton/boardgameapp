import React, { useState, useEffect } from "react";
import AdvancedListItem from "./AdvancedListItem";
import css from "../styles/AdvancedGameList.module.css";
import FilterInput from "./FilterInput";
import CreatableSelect from 'react-select/creatable';
import { GENRE_OPTIONS } from "../docs/genres";
import axios from "axios";
import { buildQueries } from "@testing-library/react";

const AdvancedGameList = () => {
	const [searchQuery, setSearchQuery] = useState("");
	const [noOfPlayers, setNoOfPlayers] = useState("");
	const [selectedGenres, setSelectedGenres] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [games, setGames] = useState([]);

	useEffect(() => {
		console.log("Fetch Data")
		axios.get("/api/games").then((response) => {
			setIsLoading(false);
			setGames(response.data);
		}).catch((err) => {
			setIsLoading(false);
			alert("something went wrong");
		})
	}, [])

	const filteredGames = games.filter(game => {
		const nameCheck = game.name.toLowerCase().includes(searchQuery.toLowerCase())
		const playerCheck = (noOfPlayers >= game.minPlayers && noOfPlayers <= game.maxPlayers) || noOfPlayers === ""
		const genreCheck = selectedGenres.some(selectedGenre => (game.genres).includes(selectedGenre.value)) || selectedGenres.length === 0

		return nameCheck && playerCheck && genreCheck
	})

	const onGenreSelect = (newValue) => {
		setSelectedGenres(newValue)
	}

	const styles = {
		control: styles => ({
			...styles,
			backgroundColor: 'white'
		})
	}

	return (
		<>
			<h2>Board Game Filter</h2>
			{
				isLoading ? (
					<p>loading...</p>
				) : (
					<>
						<FilterInput
							label="Game"
							type="text"
							placeholder="Search for game..."
							value={searchQuery}
							onChange={setSearchQuery}
						/>
						<FilterInput
							label="Players"
							type="number"
							placeholder="Number of players..."
							value={noOfPlayers}
							onChange={setNoOfPlayers}
							min={1}
							max={12}
						/>

						<div className="category-list">
							<label>Categories</label>
							<CreatableSelect
								isMulti
								onChange={onGenreSelect}
								options={GENRE_OPTIONS}
								classNamePrefix="react-select"
							/>
						</div>

						<ul className={css.gameList}>
							{filteredGames.map(game => (
								<AdvancedListItem
									key={game._id}
									name={game.name}
									minPlayers={game.minPlayers}
									maxPlayers={game.maxPlayers}
									noOfPlays={game.noOfPlays}
									gameDescription={game.description}
								/>
							))}
						</ul>
					</>
				)
			}

		</>
	)
}

export default AdvancedGameList;