import React, { useCallback, useState } from 'react';

const LargeTextArea = (props) => {
const { value, onChange, label } = props;

return (
    <div className="form__field">
    <label htmlFor="input">{label}</label>
    <div className="form__field-description">
    <textarea
      value={value}
      onChange={
          (event) => onChange(event.target.value)
      }
    />
  </div>
  </div>
)
}

export default LargeTextArea;