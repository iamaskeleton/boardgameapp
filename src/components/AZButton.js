import React, {useState} from 'react';
import closeIcon from "../img/close.png"

const ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ#"

const AZButton = (props) => {
  const {onLetterClicked, letterFilter} = props
  const [isMenuOpen, setIsMenuOpen] = useState(false)

  if(isMenuOpen === false) {
    return(
      <button
        type="button"
        className="az__button"
        onClick={() => setIsMenuOpen(true)}
      >
        A-Z
      </button>
    )
  }

  return(
    <>
      <button
        type="button"
        className="az__button-letter az__button-close"
        onClick={() => setIsMenuOpen(false)}
      >
        <img src={closeIcon} alt="Close Button"/>
      </button>
      <div className="az__dropdown">
        {ALPHABET.split("").map(letter => (
          <button 
            type="button"
            className={`az__button-letter ${letter === letterFilter ? "az__button-letter-active" : "" }`} // back tics allow you to put javascript int the middle of it
            onClick={() => onLetterClicked(letter === letterFilter ? "" : letter)}
          >
            {letter}
          </button>
        ))}
      </div>
    </>
  )
}

export default AZButton