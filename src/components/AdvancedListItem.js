import React, { useState } from "react";
import css from "../styles/AdvancedListItem.module.css"

const AdvancedListItem = (props) => {
	const {name, minPlayers, maxPlayers, noOfPlays, gameDescription} = props;
	const [playCount, setPlayCount] = useState(noOfPlays);

	return (
		<li className={css.listItem}>
			<h3>{name}</h3>
			<p>{`For ${minPlayers}-${maxPlayers} Players`}</p>

			{playCount === 0 ? (
				<p>
					<em>This game has not been played</em>
				</p>
			) : (
				<p>{`I've played this game ${playCount} time${playCount === 1 ? "" : "s"}`}</p>
			)}

			<p>{gameDescription}</p>
			
			<button
				type="button"
				onClick={
					() => setPlayCount(playCount+1)
				}
			>
				Add Play
			</button>
		</li>
	)
}

export default AdvancedListItem;