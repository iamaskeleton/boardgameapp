import React, { useState, useEffect } from "react";
import axios from "axios";

const GameDetails = (props) => {
    const {gameID} = props;
    const [gameData, setGameData] = useState("");

    useEffect(() => {
    //when gameID changes - fetch data
        axios.get(`/api/game?id=${gameID}`).then((response) => { // ${gameID} changes depending on what is sent through from the gameID property in BasicGameList
            console.log(response.data)
            const gameInfo = response.data // as it is a single object, not an array, dont need brackets
            setGameData(gameInfo) // could also have written setGameData(response.data)
        })
	}, [gameID])

    console.log(gameData);

    return(
        <p>{gameData.longDescription}</p> // because the whole object is called in the state, it means we can define whatever property we cant after the state is referenced.

    )
};

export default GameDetails;