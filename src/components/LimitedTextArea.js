import React, { useCallback, useState } from 'react';

const LimitedTextarea = (props) => {
	const LIMIT = 120;
	const { value, onChange, label } = props;
  
  const onInputChange = (val) => {
  	if (val.length > LIMIT) return;
    onChange(val);
  }
  
	return (
  	<div className="form__field">
      <label htmlFor="input">{label}</label>
	  <div className="form__field-description">
      <textarea
        value={value}
        onChange={
			(event) => onInputChange(event.target.value)
		}
      />
      <p>{`${LIMIT - value.length} characters remaining`}</p>
    </div>
	</div>
  )
}

export default LimitedTextarea;