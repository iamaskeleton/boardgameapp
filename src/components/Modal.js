import React from 'react';
import css from '../styles/Modal.module.css'
import closeIcon from '../img/close.png'

const Modal = (props) => {
	const { children, isOpen, onCloseButtonClick } = props;
	if (isOpen === false)
		return null
	return (
		<div className={css.modal}>
			<div className={css.modalBody}>
				{
					children
				}

				<button
					type="button"
					onClick={onCloseButtonClick}
				>
					<img src={closeIcon} alt="Close Button" />
				</button>
			</div>
		</div>
	)
}

export default Modal