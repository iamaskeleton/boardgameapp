import React from "react";

const ComponentName = () => {
    //useState/useEffect logic in here

    return (
        <p>Hello world!</p>
    )
}

export default ComponentName;